const productModel = require("../models/productModel");

const productController = {
  getAll: async function (req, res) {
    console.log(req.query.category);
    let products;
    if (req.query.category) {
      products = await productModel.getProductsByCategory(req.query.category);
    } else {
      products = await productModel.getAllProducts();
    }
    res.status(200).json(products);
  },

  getById: async function (req, res) {
    let product = await productModel.getProductById(req.params.id);
    res.status(200).json(product);
  },

  add: async function (req, res) {
    let addedProduct = await productModel.addProduct(req.body);
    res.status(201).json(addedProduct);
  },

  edit: async function (req, res) {
    let editedProduct = await productModel.editProduct(req.params.id, req.body);
    res.status(200).json(editedProduct);
  },

  delete: async function (req, res) {
    let deletedProduct = await productModel.deleteProduct(req.params.id);
    res.status(200).json(deletedProduct);
  },
};

module.exports = productController;
