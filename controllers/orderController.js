const orderModel = require("../models/orderModel");

const orderController = {
  getAll: async function (req, res) {
    let orders;
    orders = await orderModel.getAllOrders();
    res.status(200).json(orders);
  },

  getById: function (req, res) {
    let order = orderModel.getOrderById(Number(req.params.id));
    res.status(200).json(order);
  },

  add: async function (req, res) {
    let addedOrder = await orderModel.addOrder(req.body);
    res.status(201).json(addedOrder);
  },

  edit: async function (req, res) {
    let editedOrder = await orderModel.editOrder(req.params.id, req.body);
    res.status(200).json(editedOrder);
  },

  delete: async function (req, res) {
    let deletedOrder = await orderModel.deleteOrder(req.params.id);
    res.status(200).json(deletedOrder);
  },
};

module.exports = orderController;
