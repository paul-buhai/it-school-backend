const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchma = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "User", required: true },
  products: [{ type: Schema.Types.ObjectId, ref: "Product", required: true }],
  date: { type: String, default: new Date().toString() },
  status: { type: String, default: "Order placed" },
});

orderSchma.virtual("total").get(function () {
  return this.products.reduce((sum, currentProduct) => {
    return sum + currentProduct.price;
  }, 0);
});

module.exports = mongoose.model("Order", orderSchma);
