const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema({
  name: { type: String, required: true },
  price: { type: Number, required: true },
  picture: { type: String, default: "" },
  category: { type: String, default: "" },
});

module.exports = mongoose.model("Product", productSchema);
