const express = require("express");
const app = express();
const mongoose = require("mongoose");

const productRoutes = require("./routes/productRoutes");
const userRoutes = require("./routes/userRoutes");
const orderRoutes = require("./routes/orderRoutes");

app.use(express.json());

app.get("/", (req, res) => res.send("It's working"));

app.use("/products", productRoutes);
app.use("/users", userRoutes);
app.use("/orders", orderRoutes);

mongoose
  .connect(
    "mongodb+srv://paul:123@it-school-nqh4n.mongodb.net/test?retryWrites=true&w=majority",
    { useNewUrlParser: true }
  )
  .then(() => {
    app.listen(4000);
  });
