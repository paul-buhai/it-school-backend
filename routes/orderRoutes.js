const express = require("express");
const orderRouter = express.Router();

const orderController = require("../controllers/orderController");

orderRouter.get("/", orderController.getAll);

orderRouter.get("/:id", orderController.getById);

orderRouter.post("/", orderController.add);

orderRouter.put("/:id", orderController.edit);

orderRouter.delete("/:id", orderController.delete);

module.exports = orderRouter;
