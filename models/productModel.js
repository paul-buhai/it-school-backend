const productsDB = require("../schemas/productSchema");

const productModel = {
  getAllProducts: async function () {
    return await productsDB.find();
  },

  getProductById: async function (id) {
    return await productsDB.findById(id);
  },

  getProductsByCategory: async function (category) {
    return await productsDB.find({
      category: { $regex: category, $options: "i" },
    });
  },

  addProduct: async function (product) {
    return await productsDB.create(product);
  },

  editProduct: async function (id, product) {
    return await productsDB.findByIdAndUpdate(id, product, { new: true });
  },

  deleteProduct: async function (id) {
    let { deletedCount } = await productsDB.deleteOne({ _id: id });
    return deletedCount;
  },
};

module.exports = productModel;
