const ordersDB = require("../schemas/orderSchema");

const orderModel = {
  getAllOrders: async function () {
    return await ordersDB
      .find()
      .populate({
        path: "user",
        select: "firstName lastName email",
      })
      .populate({ path: "products", select: "name price" });
  },

  getOrderById: async function (id) {
    return await ordersDB
      .findById(id)
      .populate({
        path: "user",
        select: "firstName lastName email",
      })
      .populate({ path: "products", select: "name price" });
  },

  addOrder: async function (order) {
    return await ordersDB.create(order);
  },

  editOrder: async function (id, order) {
    return await ordersDB.findByIdAndUpdate(id, order, { new: true });
  },

  deleteOrder: async function (id) {
    let { deletedCount } = await ordersDB.deleteOne({ _id: id });
    return deletedCount;
  },
};

module.exports = orderModel;
