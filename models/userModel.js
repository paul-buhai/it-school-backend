const usersDB = require("../schemas/userSchema");

const userModel = {
  getAllUsers: async function () {
    return await usersDB.find();
  },

  getUserById: async function (id) {
    return await usersDB.findById(id);
  },

  getUserByName: async function (name) {
    return await usersDB.find({
      $or: [
        { firstName: { $regex: name, $options: "i" } },
        { lastName: { $regex: name, $options: "i" } },
      ],
    });
  },

  addUser: async function (user) {
    return await usersDB.create(user);
  },

  editUser: async function (id, user) {
    return await usersDB.findByIdAndUpdate(id, user, { new: true });
  },

  deleteUser: async function (id) {
    let { deletedCount } = await usersDB.deleteOne({ _id: id });
    return deletedCount;
  },
};

module.exports = userModel;
